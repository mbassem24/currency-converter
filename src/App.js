import React from 'react'
import RatesInfoWrap from './app/modules/RatesInformation/RatesInfoWrap'
import ScrollReverse from './app/shell/ScrollReverse'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import FooterNav from './app/shell/layout/FooterNav';
import RatesConvertWrap from './app/modules/RatesConvert/RatesConvertWrap';
import RatesConvertEnhanceWrap from './app/modules/RatesConvertEnhance/RatesConvertEnhanceWrap';

function App() {
    return (
        <>
            
            <Router>
                <ScrollReverse />

                <div className="app-contain">
                    <div className="app-wrap">
                        <div className="app-data">
                            <div className="app-data-body">
                                <Switch>
                                    <Route
                                        exact
                                        path="/"
                                        component={RatesInfoWrap}
                                    />

                                    <Route
                                        exact
                                        path="/currency-conversion"
                                        component={RatesConvertWrap}
                                    />

                                    <Route
                                        exact
                                        path="/currency-conversion-2"
                                        component={RatesConvertEnhanceWrap}
                                    />
                                </Switch>
                            </div>
                            <div className="app-data-foot">
                                <FooterNav />
                            </div>

                        </div>
                    </div>
                </div>
                
                
            
            </Router>

        </>
    )
}

export default App
