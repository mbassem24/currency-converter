import React, { useEffect, useState } from 'react'
import CustomSelectInput from '../../shell/Inputs/CustomSelectInput'
import CustomNumberInput from '../../shell/Inputs/CustomNumberInput'
import axios from 'axios'

function RatesConvertEnhanceWrap() {

    const [coverterData, setConverterData] = useState({
        amount: "",
        from: "AUD",
        to: "BGN",
    })
    const [currencies, setCurrenciesList] = useState({loading: true, data: [], error: false})

    useEffect(() => {
        axios.get('https://api.frankfurter.app/currencies')
            .then(response => {
                setCurrenciesList({
                    loading: false,
                    data: [...Object.keys(response.data)],
                    error: false
                })
            })
            .catch(error => {
                console.log(error)
                setCurrenciesList({
                    loading: false,
                    data: {},
                    error: true
                })
            })
    }, [])



    //- calc the currency
    const [amountResult, setAmountResult] = useState({isActive: false, data: {}})

    useEffect(() => {
        if(coverterData.amount !== '') {
            axios.get(`https://api.frankfurter.app/latest?amount=${+coverterData.amount}&from=${coverterData.from}&to=${coverterData.to}`)
                .then(response => {
                    console.log(response.data)
                    setAmountResult({
                        isActive: true,
                        data: response.data
                    })

                })
                .catch(error => {
                    console.log(error)
                    setAmountResult({
                        isActive: false,
                        data: {}
                    })

                })
        }
       
    }, [coverterData])


    return (
        <div className="app-data-info">
            <div className="app-info-head converter">
                <div className="app-info-currency-flag main-flag white">
                    <p>Currency Conversion 2</p>
                </div>
            </div>
            
            
            <div className="app-info-body converter enhance">
                {
                    currencies.loading
                    ?   <>
                            <div className="card-loader data"></div>
                            <div className="card-loader data"></div>

                        </>
                    :   currencies.error
                        ?   <p>OPPS!</p>
                        :   <>
                                <div className="rates-convert-items-wrap">
                                    <div className="rates-convert-item full flex">
                                        <div className="input-wrap">
                                            <CustomNumberInput
                                                data={coverterData}
                                                setData={setConverterData}
                                                name="amount"
                                                placeHolder="0.00"
                                            />
                                        </div>
                                        <div className="input-wrap">
                                            <CustomSelectInput 
                                                item={currencies.data} 
                                                data={coverterData}
                                                setData={setConverterData}
                                                type="from"
                                                hideIcon
                                            />
                                        </div>
                                    </div>
                                    <div className="rates-convert-item full flex">
                                        <div className="input-wrap">
                                            <input 
                                                className="site-input" 
                                                type="text"
                                                value={amountResult.isActive ? amountResult.data.rates[Object.keys(amountResult.data.rates)[0]] : '0.00'}
                                                onChange={() => null}
                                                disabled 

                                            />
                                        </div>
                                        <div className="input-wrap">
                                            <CustomSelectInput 
                                                item={currencies.data} 
                                                data={coverterData}
                                                setData={setConverterData}
                                                type="to"
                                                hideIcon

                                            />
                                        </div>
                                    </div>
                                </div>
                            </>
                }

                
                

            </div>
        </div>
    )
}

export default RatesConvertEnhanceWrap
