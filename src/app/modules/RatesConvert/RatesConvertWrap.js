import React, { useEffect, useState } from 'react'
import CustomSelectInput from '../../shell/Inputs/CustomSelectInput'
import CustomNumberInput from '../../shell/Inputs/CustomNumberInput'

import axios from 'axios'

function RatesConvertWrap() {

    const [coverterData, setConverterData] = useState({
        amount: "",
        from: "AUD",
        to: "BGN",
    })
    const [currencies, setCurrenciesList] = useState({loading: true, data: [], error: false})

    useEffect(() => {
        axios.get('https://api.frankfurter.app/currencies')
            .then(response => {
                setCurrenciesList({
                    loading: false,
                    data: [...Object.keys(response.data)],
                    error: false
                })
            })
            .catch(error => {
                console.log(error)
                setCurrenciesList({
                    loading: false,
                    data: {},
                    error: true
                })
            })
    }, [])


    //- calc the currency
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [amountResult, setAmountResult] = useState({isActive: false, data: {}})
    const calculateCurrency = () => {
        if(coverterData.amount === '') {
            return
        }
        setIsSubmitted(true)

        axios.get(`https://api.frankfurter.app/latest?amount=${+coverterData.amount}&from=${coverterData.from}&to=${coverterData.to}`)
            .then(response => {
                console.log(response.data)
                setIsSubmitted(false)
                setAmountResult({
                    isActive: true,
                    data: response.data
                })

            })
            .catch(error => {
                console.log(error)
                setIsSubmitted(false)
                setAmountResult({
                    isActive: false,
                    data: {}
                })

            })
    }



    return (
        <div className="app-data-info">
            <div className="app-info-head converter">
                <div className="app-info-currency-flag main-flag white">
                    <p>Currency Conversion</p>
                </div>
            </div>
            
            
            <div className="app-info-body converter">
                {
                    currencies.loading
                    ?   <>
                            <div className="card-loader data"></div>
                            <div className="card-loader data"></div>
                            <div className="card-loader data"></div>
                            <div className="card-loader data"></div>

                        </>
                    :   currencies.error
                        ?   <p>OPPS!</p>
                        :   <>
                                
                                <div className="rates-convert-items-wrap">
                                    <div className="rates-convert-item">
                                        <div className="input-wrap">
                                            <label>From</label>
                                            <CustomSelectInput
                                                item={currencies.data} 
                                                data={coverterData}
                                                setData={setConverterData}
                                                type="from"
                                            />
                                        </div>
                                    </div>
                                    <div className="rates-convert-item">
                                        <div className="input-wrap">
                                            <label>To</label>

                                            <CustomSelectInput
                                                item={currencies.data} 
                                                data={coverterData}
                                                setData={setConverterData}
                                                type="to"
                                            />
                                            
                                        </div>
                                    </div>
                                </div>
                                <div className="rates-convert-items-wrap">
                                    <div className="rates-convert-item full">
                                        <div className="input-wrap">
                                            <label>Amount</label>
                                            <CustomNumberInput
                                                data={coverterData}
                                                setData={setConverterData}
                                                name="amount"
                                            />
                                        </div>
                                    </div>
                
                                </div>
                                <div className="rates-convert-items-wrap">
                                    <div className="rates-convert-item full">
                                        <button 
                                            className={`site-btn ${coverterData.amount === '' ? 'disabled' : ''}`} 
                                            onClick={calculateCurrency}>
                                                <span>Calculate Now!</span>
                                                {
                                                    isSubmitted
                                                    ?   <div className="spinner"></div>
                                                    :   null
                                                }
                                        </button>
                                    </div>
                                </div>
                            </>
                }

                {
                    amountResult.isActive
                    ?   <div className="amount-result">
                            <strong>
                                {
                                    `${amountResult.data.amount} ${amountResult.data.base}`
                                }
                            </strong>
                            <span>=</span>
                            <strong>
                                {
                                    `${amountResult.data.rates[Object.keys(amountResult.data.rates)[0]]} ${Object.keys(amountResult.data.rates)[0]}`
                                }
                            </strong>

                        </div>
                    :   null
                }
                
                

            </div>
        </div>
    )
}

export default RatesConvertWrap
