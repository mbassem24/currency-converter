import React, {useEffect, useState} from 'react'
import CurrencyFlag from 'react-currency-flags';
import RatesInfoItem from './RatesInfoItem';
import axios from 'axios';
import uuid from 'react-uuid'

function RatesInfoWrap() {
    const [latestRates, setLatestRates] = useState({loading: true, data: [], base: "", error: false})
    
    
    useEffect(() => {
        //axios.get(`http://data.fixer.io/api/latest?access_key=788b95febbf1b971bf58a473f64f8d83`)
        axios.get(`https://api.frankfurter.app/latest?from=USD`)
            .then(response => {
                let ratesArray = [];
                Object.keys(response.data.rates)
                    .forEach(function eachKey(key) { 
                        ratesArray.push({id: uuid(), title: key, rate: response.data.rates[key]})
                    });
                setLatestRates({
                    loading: false,
                    data: ratesArray,
                    base: response.data.base,
                    error: false
                })
            })
            .catch(error => {
                console.log(error)
                setLatestRates({
                    loading: false,
                    data: [],
                    base: "",
                    error: true
                })
            })
    }, [])


    return (
        <div className="app-data-info">
            <div className="app-info-head">
                <div className="app-info-currency-flag main-flag white">
                    {
                        latestRates.loading
                        ?   <>
                                <div className="card-loader image"></div>
                            </>
                        :   latestRates.error
                            ?   <p>OPPS!</p>
                            :   <>
                                    <CurrencyFlag currency={latestRates.base} />
                                    <p>{latestRates.base}</p>
                                </>
        
                    }
                </div>
            </div>
            <div className="app-info-body">
                <div className="app-info-list">
                    {
                        latestRates.loading
                        ?   <>
                                <div className="card-loader data"></div>
                                <div className="card-loader data"></div>
                                <div className="card-loader data"></div>
                                <div className="card-loader data"></div>
                                <div className="card-loader data"></div>
                                <div className="card-loader data"></div>
                            </>
                        :   latestRates.error
                            ?   <p>OPPS!</p>
                            :   latestRates.data.map(item => {
                                    return (
                                        <RatesInfoItem key={item.id} item={item} setData={setLatestRates} />
                                    )
                                })
                    }
                </div>
            </div>
        </div>
    )
}

export default RatesInfoWrap
