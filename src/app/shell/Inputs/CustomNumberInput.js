import React from 'react'

function CustomNumberInput({data, setData, name, placeHolder}) {
    const handleInputChange = e => {
        var numbers = /^[0-9.]+$/;
        if (!e.target.value.match(numbers) && e.target.value !== '') {
            return;
        }
        setData({
            ...data,
            [name]: e.target.value
        })
    }
    return (
        <input
            className="site-input"
            type="text"
            value={data[name]}
            onChange={handleInputChange}
            placeholder={placeHolder ? placeHolder : ""}
        />
    )
}

export default CustomNumberInput
