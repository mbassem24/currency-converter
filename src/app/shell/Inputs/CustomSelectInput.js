import React from 'react'
import DownArrow from '../../../assets/media/icons/down-arrow.png';

function CustomSelectInput({item, data, setData, type, hideIcon}) {
    const hanldeSelectChange = e => {
        setData({
            ...data,
            [type]: e.target.value
        })
    }
    return (
        <>
            <select className="site-input" value={type === "from" ? data.from : data.to} onChange={hanldeSelectChange}>
                {
                    item.map((item, index) => {
                        return (
                            <option key={index} value={item}>{item}</option>
                        )
                    })
                }
            </select>
            {
                !hideIcon
                ?   <img src={DownArrow} alt="arrow-icon" />
                :   null

            }
        </>
    )
}

export default CustomSelectInput
