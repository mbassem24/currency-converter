import React from 'react'
import { useLocation, Link } from "react-router-dom";

function FooterNav() {
    let { pathname } = useLocation();

    return (
        <ul className="foot-nav">
            <li className={`foot-nav-item ${pathname === '/' ? 'active' : ''}`}>
                <Link to="/">
                    <span>Rates</span>
                </Link>
            </li>
            <li className={`foot-nav-item ${pathname === '/currency-conversion' ? 'active' : ''}`}>
                <Link to="/currency-conversion">
                    <span>Conversion</span>
                </Link>
            </li>

            <li className={`foot-nav-item ${pathname === '/currency-conversion-2' ? 'active' : ''}`}>
                <Link to="/currency-conversion-2">
                    <span>Conversion 2</span>
                </Link>
            </li>
        </ul>
    )
}

export default FooterNav
